.text
.globl _start

_start:
    mov $100, %rdi
    call sleep@PLT

    movq $0, %rdi
    call exit@PLT
    