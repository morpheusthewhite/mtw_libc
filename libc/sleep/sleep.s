.data
SYSCALL_NANOSLEEP=35

.text

.globl  sleep
.type   sleep, @function

sleep:
    # building the needed struct 
    pushq $0        # n of nanoseconds
    pushq %rdi        # n of seconds
    mov %rsp, %rdi  # struct address
    xor %rsi, %rsi  # rem

    mov $SYSCALL_NANOSLEEP, %rax
    syscall 

    sub $16, %rsp # clearing the stack
    ret
