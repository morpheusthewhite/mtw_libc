.data 

msg:
    .string "Hello World!\n"
    len = .-msg

STDOUT = 1
SYSCALL_WRITE = 1
SYSCALL_EXIT = 60

.text
    .global _start
_start:
    movq $STDOUT, %rdi # fd
    leaq msg(%rip), %rsi # string to be printed, using lea to support PIC
    movq $len, %rdx # length of the string
    movq $SYSCALL_WRITE, %rax # syscall number
    syscall

    mov $SYSCALL_EXIT, %rax       
    mov $0, %rdi     # exit code
    syscall
