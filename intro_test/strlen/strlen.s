.globl  mStrlen
.type   mStrlen, @function # needed to create function symbol

mStrlen:
    movq $0, %rax
beginLoop:
    movb (%rdi, %rax, 1), %dl
    testb %dl, %dl
    jz endLoop
    incq %rax
    jmp beginLoop
endLoop:
    ret
    