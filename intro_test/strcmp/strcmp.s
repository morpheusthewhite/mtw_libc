.globl  mStrcmp
.type   mStrlen, @function # needed to create function symbol

mStrcmp:
    movq $0, %rax
    movq $0, %rcx
beginLoop:
    movb (%rdi, %rcx, 1), %dl
    movb (%rsi, %rcx, 1), %dh
    cmp %dl, %dh
    jnz different # if the current letter is different

    incq %rcx
    testb %dl, %dl 
    jz endLoop # if both string are at the end
    jmp beginLoop

different:
    jc secondGreater
    movq $-1, %rax # if dh > dl
    jmp endLoop

secondGreater:
    movq $1, %rax # if dh < dl

endLoop:
    ret
