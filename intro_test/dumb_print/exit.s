.globl exit
.type  exit, @function

.data 
SYSCALL_EXIT = 60

.text 
exit:
    mov $SYSCALL_EXIT, %rax
    syscall

    ret
    