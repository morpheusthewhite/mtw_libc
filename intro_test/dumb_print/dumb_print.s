.data
STDOUT = 1
SYSCALL_WRITE = 1

.text

.globl d_print
.type  d_print, @function

# receive a string and print it in the stdout
d_print:
    # calculating string length
    push %rdi
    call mStrlen@PLT
    pop %rdi

    movq %rdi, %rsi # char* s
    movq $STDOUT, %rdi # fd
    movq %rax, %rdx # strlen(s)
    movq $SYSCALL_WRITE, %rax # syscall number
    syscall

    ret
    