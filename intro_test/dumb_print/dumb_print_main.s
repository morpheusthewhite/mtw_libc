
.data
    
msg:
    .string "Hello from a print!\n"

.text
.globl _start

_start:
    leaq msg(%rip), %rdi
    call d_print@PLT

    movq $0, %rdi
    call exit@PLT
